<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class HealthController extends AbstractController
{

    protected const FOUR_GB = 4294967296;

    /**
    * @Route("/health")
    */
    public function index(EntityManagerInterface $entityManager): Response
    {
        try {
            // Check DB connection
            $entityManager->getConnection()->executeQuery('SELECT 1');

            // Check available space
            $this->checkFreeSpace();

            // Ok
            return new JsonResponse(['status' => 'ok']);
        } catch (\Exception|\Error $e) {
            return new JsonResponse([
                'status' => 'nok',
                'message' => $e->getMessage()
            ], Response::HTTP_SERVICE_UNAVAILABLE);
        }
    }

    /**
     * @throws \Exception
     */
    protected function checkFreeSpace(): void
    {
        $freeSpace = disk_free_space('/');
        if ($freeSpace === false || $freeSpace < self::FOUR_GB) {
            throw new \Exception('Space is running out');
        }
    }
}