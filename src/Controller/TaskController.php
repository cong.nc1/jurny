<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/task")
 */
class TaskController extends AbstractController
{

    /**
     * @Route("/", name="app_task_index", methods={"GET"})
     */
    public function index(TaskRepository $taskRepository): Response
    {
        $user = $this->getUser();
        if (is_null($user)) {
            return $this->redirectToRoute('app_login', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('task/index.html.twig', [
            'tasks' => $taskRepository->findBy(['creator' => $user->getId()],['dueDate'=> 'asc']),
        ]);
    }

    /**
     * @Route("/new", name="app_task_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TaskRepository $taskRepository): Response
    {
        $task = new Task();
        $task->setCreator($this->getUser());
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $taskRepository->add($task, true);

            return $this->redirectToRoute('app_task_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('task/new.html.twig', [
            'task' => $task,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{uuid}", name="app_task_show", methods={"GET"})
     */
    public function show(string $uuid, TaskRepository $taskRepository): Response
    {
        $task = $taskRepository->getTaskByUuid($uuid);
        if (is_null($task)) {
            return new Response("Not found");
        }
        return $this->render('task/show.html.twig', [
            'task' => $task,
        ]);
    }

    /**
     * @Route("/{uuid}/edit", name="app_task_edit", methods={"GET", "POST"})
     */
    public function edit(string $uuid, Request $request, TaskRepository $taskRepository): Response
    {
        $task = $taskRepository->getTaskByUuid($uuid);
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $taskRepository->add($task, true);

            return $this->redirectToRoute('app_task_index', [], Response::HTTP_SEE_OTHER);
        }

        if (is_null($task)) {
            return new Response("Not found");
        }

        return $this->renderForm('task/edit.html.twig', [
            'task' => $task,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{uuid}", name="app_task_delete", methods={"POST"})
     */
    public function delete(string $uuid, Request $request, TaskRepository $taskRepository): Response
    {
        $task = $taskRepository->getTaskByUuid($uuid);

        if ($task && $this->isCsrfTokenValid('delete'.$task->getTaskUuid(), $request->request->get('_token'))) {
            $taskRepository->remove($task, true);
        }

        return $this->redirectToRoute('app_task_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/{uuid}/completed", name="app_task_completed", methods={"POST"})
     */
    public function markCompleted(string $uuid, Request $request, TaskRepository $taskRepository): Response
    {
        $task = $taskRepository->getTaskByUuid($uuid);

        if ($task && $this->isCsrfTokenValid('update'.$task->getTaskUuid(), $request->request->get('_token'))) {
            $taskRepository->markCompleted($task);
        }

        return $this->redirectToRoute('app_task_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/{uuid}/incompleted", name="app_task_incompleted", methods={"POST"})
     */
    public function markInCompleted(string $uuid, Request $request, TaskRepository $taskRepository): Response
    {
        $task = $taskRepository->getTaskByUuid($uuid);

        if ($task && $this->isCsrfTokenValid('update'.$task->getTaskUuid(), $request->request->get('_token'))) {
            $taskRepository->markInCompleted($task);
        }

        return $this->redirectToRoute('app_task_index', [], Response::HTTP_SEE_OTHER);
    }
}
