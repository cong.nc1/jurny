<?php

namespace App\Controller;

use Pettibon\AppBundle\Entity\Activity;
use Pettibon\AppBundle\Mapper\Request\Task\TaskMapper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{
    /**
    * @Route("/", name="home")
    */
    public function index(): Response
    {
        return $this->redirectToRoute('app_task_index');

    }
}