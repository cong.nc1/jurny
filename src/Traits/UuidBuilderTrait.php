<?php

namespace App\Traits;

use Symfony\Component\Uid\Uuid;

trait UuidBuilderTrait
{
    /**
     * @return string
     */
    public function buildUuid(): string
    {
        return Uuid::v4();
    }
}
