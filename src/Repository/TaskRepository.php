<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Task>
 *
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    /**
     * To update or add new task
     *
     * @param Task $entity
     * @param bool $flush
     * @return void
     */
    public function add(Task $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * To remove task
     *
     * @param Task $entity
     * @param bool $flush
     * @return void
     */
    public function remove(Task $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Update task to completed
     *
     * @param Task $entity
     * @return void
     */
    public function markCompleted(Task $entity): void
    {
        $entity->setIsCompleted(true);
        $this->add($entity, true);
    }

    /**
     * Update task to incomplete
     *
     * @param Task $entity
     * @return void
     */
    public function markInCompleted(Task $entity): void
    {
        $entity->setIsCompleted(false);
        $this->add($entity, true);
    }

    /**
     * Allow get task by uuid
     *
     * @param string $taskUuid
     * @return Task|null
     * @throws NonUniqueResultException
     */
    public function getTaskByUuid(string $taskUuid): ?Task
    {
        return $this
            ->createQueryBuilder('T')
            ->where('T.taskUuid = :taskUuid')
            ->setParameter('taskUuid', $taskUuid)
            ->getQuery()
            ->getOneOrNullResult();
    }



//    /**
//     * @return Task[] Returns an array of Task objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Task
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
