<?php

namespace App\Test;

use App\Entity\Task;
use App\Entity\User;

use PHPUnit\Framework\TestCase;

class TaskTest extends TestCase
{
    public function testCreatedAt()
    {
        $task = new Task();
        $task->onNew();
        $this->assertInstanceOf(\DateTime::class, $task->getCreatedAt());
    }

    public function testCreatedAtFromRequest()
    {
        $task = new Task();
        $task->setCreatedAt(new \DateTime('2023-03-30 15:50:35'));
        $task->onNew();
        $this->assertInstanceOf(\DateTime::class, $task->getCreatedAt());
        $this->assertEquals(new \DateTime('2023-03-30 15:50:35'), $task->getCreatedAt());
    }

    public function testProperty()
    {
        $user = $this->createMock(User::class);
        $user
            ->method('getId')
            ->willReturn(1);

        $task = (new Task())->setCreator($user);
        $this->assertEquals(1, $task->getCreator()->getId());
    }
}
